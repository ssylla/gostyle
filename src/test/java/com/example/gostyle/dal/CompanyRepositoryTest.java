package com.example.gostyle.dal;

import com.example.gostyle.domain.Company;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CompanyRepositoryTest {
	
	@Autowired
	CompanyRepository dao;
	
	@Test
	void findByNumTVA() {
		
		// Préparation
		Company company = new Company( "Sebsy.Corp", "NANTES", "NANTES" );
		company.setNumTVA( "monNum" );
		Company storedCompany = dao.save( company );
		
		// Exécution de l'action
		Company foundCompany = dao.findByNumTVA( "monNum" );
		
		// Attentes
		assertEquals( storedCompany.getId(), foundCompany.getId() );
	}
}