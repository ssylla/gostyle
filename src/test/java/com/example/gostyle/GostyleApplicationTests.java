package com.example.gostyle;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class GostyleApplicationTests {
	
	@LocalServerPort
	private int port;
	
	@Autowired
	private TestRestTemplate testRestTemplate;
	
	@Test
	void testMyIndicator() {
		
		// Préparation
		String uri = String.format( "http://localhost:%d/magnt/health/myIndicator", port);
		// Action
		ResponseEntity<String> responseEntity = testRestTemplate.getForEntity( uri, String.class );
		// Attentes
		assertEquals(responseEntity.getStatusCode(), HttpStatus.OK );
	}
}
