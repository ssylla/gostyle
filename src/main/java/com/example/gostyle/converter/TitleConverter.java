package com.example.gostyle.converter;

import com.example.gostyle.domain.Title;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class TitleConverter implements AttributeConverter<Title, String> {
	@Override
	public String convertToDatabaseColumn( Title title ) {
		return null;
	}
	
	@Override
	public Title convertToEntityAttribute( String s ) {
		return null;
	}
}
