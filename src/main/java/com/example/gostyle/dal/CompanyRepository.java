package com.example.gostyle.dal;

import com.example.gostyle.domain.Company;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Collection;

@RepositoryRestResource( path = "soc", exported = false )
public interface CompanyRepository extends CrudRepository<Company, Long> {
	
	@RestResource( path = "by-name" )
	Collection<Company> findByName( @Param( "id" ) String name );
	
	@RestResource( path = "by-name-and-addr" )
	Collection<Company> findByNameAndAddress( String name, String address );
	
	@RestResource( path = "by-name-or-addr" )
	Collection<Company> findByNameOrAddress( String name, String address );
	
	@RestResource(path = "by-tva")
	Company findByNumTVA(String num);
}
