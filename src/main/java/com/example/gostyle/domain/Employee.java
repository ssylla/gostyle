package com.example.gostyle.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Employee implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	@Column(unique = false)
	private String email;
	@Enumerated(value = EnumType.ORDINAL)
	private Title title;
	
	@ManyToOne(cascade = CascadeType.ALL)
	private Company company;
	
	public Employee() {
	}
	
	public Employee( String name, String email, Title title ) {
		this.name = name;
		this.email = email;
		this.title = title;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId( Long id ) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName( String name ) {
		this.name = name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail( String email ) {
		this.email = email;
	}
	
	public Title getTitle() {
		return title;
	}
	
	public void setTitle( Title title ) {
		this.title = title;
	}
	
	public Company getCompany() {
		return company;
	}
	
	public void setCompany( Company company ) {
		if (null != this.company ) {
			this.company.getEmployeeSet().remove( this );
		}
		this.company = company;
		if (null != this.company) {
			this.company.getEmployeeSet().add( this );
		}
	}
	
	@PrePersist
	@PreUpdate
	public void updateCompany() {
		System.out.println("Update ou Insert en cours sur "+this);
		if (null != this.company) {
			this.company.getEmployeeSet().add( this );
		}
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Employee{" );
		sb.append( "id=" ).append( id );
		sb.append( ", name='" ).append( name ).append( '\'' );
		sb.append( ", email='" ).append( email ).append( '\'' );
		sb.append( ", title=" ).append( title );
		sb.append( ", company=" ).append( company );
		sb.append( '}' );
		return sb.toString();
	}
}
