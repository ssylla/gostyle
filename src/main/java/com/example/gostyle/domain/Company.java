package com.example.gostyle.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Company implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	private String address;
	@JsonIgnore
	private String city;
	@Column(unique = true)
	private String numTVA;
	@OneToMany(mappedBy = "company")
	private java.util.Set<Employee> employeeSet = new HashSet<>();
	
	public Company() {
	}
	
	public Company( String name, String address, String city ) {
		this.name = name;
		this.address = address;
		this.city = city;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId( Long id ) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName( String name ) {
		this.name = name;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress( String address ) {
		this.address = address;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity( String city ) {
		this.city = city;
	}
	
	public String getNumTVA() {
		return numTVA;
	}
	
	public void setNumTVA( String numTVA ) {
		this.numTVA = numTVA;
	}
	
	public Set<Employee> getEmployeeSet() {
		return employeeSet;
	}
	
	public void setEmployeeSet( Set<Employee> employeeSet ) {
		this.employeeSet = employeeSet;
	}
	
	public void addEmployee(Employee employee) {
		employee.setCompany( this );
	}
}
