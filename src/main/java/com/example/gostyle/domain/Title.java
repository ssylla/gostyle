package com.example.gostyle.domain;

public enum Title {
	MOE ("Maîtrise d'oeuvre"), MOA("Maîtrise d'ouvrage");
	
	private final String label;
	Title(String label) {
		this.label = label;
	}
	
	@Override
	public String toString() {
		return label;
	}
	
	public String convert() {
		return "je suis séga "+toString();
	}
}
