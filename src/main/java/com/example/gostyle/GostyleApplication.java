package com.example.gostyle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Random;

@SpringBootApplication
public class GostyleApplication {
	
	public static void main( String[] args ) {
		SpringApplication.run( GostyleApplication.class, args );
	}
	
	@Bean
	public HealthIndicator myIndicator() {
		return () -> {
			if ( new Random().nextBoolean() ) {
				return Health.up().build();
			} else {
				return Health.down().build();
			}
		};
	}
}
